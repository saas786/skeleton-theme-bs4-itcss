# Skeleton Theme for Bootstrap 4 using ITCSS, BEM and Namespaces

It was the plan to create a very simple skeleton only. It turned into a nice universal theme.  
The theme is good to use for:  

* Personal Website
* Blog
* Image website for businesses
  
I don't really like the actual modern websites having font sizes big as a skyscraper and tons of big jumbotrons all over.
The layout of this theme is in a way more classical, but not old-fashioned. 

I started this project to update my knownledge with the actual techniques and methods to develop websites. This is what you can expect from this project: 
* Education: Getting familiary with ITCSS, BEM and Namespaces for CSS
* Standards: I'm a big fan to use standards in development "things". This project helps me to follow more highly sophisticated standards.  
* Skeleton: This theme will be my boilerplate for any new projects.
* Tools: This theme gives an brief introduction into the compilation process of the sources by using **nodejs, ruby and grunt**. To play with the cool kids, you need to know these things.

The theme is build with **Bootstrap 4**. Version 4 is in it's alpha-state and available since august. I think, it's worthy to go on with the brand new toys.

# Documentation

Please continue reading by visiting the [Wiki](https://gitlab.com/martin.schaible/skeleton-theme-bs4-itcss/wikis/home) of this project.

# Used Resources

I have read tons of documentation about **Sass/SCSS** and i always stumbled over the pages of the very talented french guy called [Hugo Giraudel](http://hugogiraudel.com/about/).
His sites are very useful:

* [Sass Guidelines](http://sass-guidelin.es)
* [Getting Started with Sass](http://www.sitepoint.com/getting-started-with-sass/)
* [Github](https://github.com/HugoGiraudel)
* [SassDoc](http://sassdoc.com/)

To get familiary with the **BEM Methodology**, i recommend these sites:

* [Methodology BEM. Block, Element, Modifier](https://en.bem.info/method/)
* [Introduction to the BEM Methodology](http://webdesign.tutsplus.com/articles/an-introduction-to-the-bem-methodology--cms-19403)
* [BMindBEMding – getting your head ’round BEM syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
* [BEMIT: Taking the BEM Naming Convention a Step Further](http://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further/)
* [The BEMIT naming convention](http://www.jamesturneronline.net/beautifulweb/bemit-naming-convention.html)

Here some resources to learn how **ITCSS** works:

* [Harry Roberts](http://csswizardry.com/) talks about ITCSS in this [video](https://www.youtube.com/watch?v=1OKZOV-iLj4). It is really worthy to spend an hour to watch this show.
* The slides of this show can be viewed [here](https://speakerdeck.com/dafed/managing-css-projects-with-itcss).
* A brief introduction into ITCSS can be read [here](http://jordankoschei.com/itcss/).
* A brand new article: [ITCSS: Scalable and Maintainable CSS Architecture](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/).

The **Open/Closed Principle** is important too:  
* [The open/closed principle applied to CSS](http://csswizardry.com/2012/06/the-open-closed-principle-applied-to-css/)

Basics about using **Namespaces in CSS** are perfectly described in this article:

* [More Transparent UI Code with Namespaces](http://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces)

Bootstrap 4 Alpha documentation:

* [Bootstrap 4](http://v4-alpha.getbootstrap.com/getting-started/introduction/)
* [Quackit Bootstrap 4 Tutorial](http://examples.quackit.com/bootstrap/bootstrap_4/tutorial/)

I'm still learning this things, so probably my stuff isn't perfect.  
If you have questions, maybe you found mistakes or you like to give some +- feeback, drop me a line at [Twitter](https://twitter.com/martin_schaible).

