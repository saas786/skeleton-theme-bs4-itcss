jQuery(document).ready(function($){
	// Browser window scroll (in pixels) after which the "Scroll to top" link is shown
	var offset = 300,
	
	// Duration of the top scrolling animation (in ms)
	scrollTopDuration = 700,
	
	// Grab the "Scroll to top" link
	$scrollTopCSSClass = $('.u-scroll-top');

	// Hide or show the "Scroll to top" link
	$(window).scroll(function(){
		// Without fade out
		( $(this).scrollTop() > offset ) ? $scrollTopCSSClass.addClass('u-scroll-top_is-visible') : $scrollTopCSSClass.removeClass('u-scroll-top_is-visible');
	});

	// Smooth scroll to top
	$scrollTopCSSClass.on('click', function(event){
		event.preventDefault();

		// Remove the focus from the link
		$(this).blur();
		
		$('body,html').animate({
				scrollTop: 0 ,
		 	},
		 	scrollTopDuration
		);
	});
});
