// =============================================================================
//                 (c) Copyright 2015 by netfusion GmbH, Küsnacht
// =============================================================================
// Version   : 0.1.0
// Created at: 01.02.16 by : Martin Schaible
// Project   : Skeleton Bootstrap 4 | ITCSS | BEM | Namespaces
// =============================================================================

module.exports = function (grunt) {
	'use strict';

	// Project configuration
	grunt.initConfig( {
		// Read the package.json
		pkg: grunt.file.readJSON('package.json'),

		// Configuration
		config: {
			// Have the cache locally, default would be on the server
			sassCacheDir: 'D:/Temp/sass-cache',

			// The source directory which hold all the source files for SCSS
			scssSrcDir: 'scss',
			// Set the source SCSS file
			scssSrcFile: '<%= config.scssSrcDir %>/main.scss',
						
			// The target directory for the compiled css files
			cssDestDir: 'prototype/assets/css',
		
			// Set the target CSS file
			cssDestFile: '<%= config.cssDestDir %>/main.css',
			cssDestFileMinified: '<%= config.cssDestDir %>/main.min.css',

			// Set the source for the HTML partial files
			htmlSrcDir: 'prototype/html',
			htmlDestDir: 'prototype',
		},

		banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
				'<%= grunt.template.today("dd.mm.yy") %>\n' +
				'* Copyright (c) <%= grunt.template.today("yyyy") %> ',

		// Task configuration
		// =====================================================================
		// Reference: http://gruntjs.com/configuring-tasks
		
		// Task for compiling the SCSS source 
		// ---------------------------------------------------------------------
		// Reference: https://github.com/sindresorhus/grunt-sass

		// Optimized for grunt-sass
		// -> grunt-contrib-sass might cause problems with the option sourcemap
		sass: {
			// Task for development, no compression
			development: {
				options: {
					cacheLocation: '<%= config.sassCacheDir %>',
					sourceMap: false
				},

				files: {
					'<%= config.cssDestFile %>':'<%= config.scssSrcFile %>'
				}
			},
	
			// Task for Distribution/Production
			distribution: {
				options: {
					cacheLocation: '<%= config.sassCacheDir %>',
					sourceMap: false
				},

				files: {
					'<%= config.cssDestFile %>':'<%= config.scssSrcFile %>'
				}
			}
		},

		// Task for Autoprefixing the css file(s) 
		// ---------------------------------------------------------------------
		// Reference: https://github.com/postcss/postcss
		//			  https://github.com/postcss/autoprefixer
		//			  https://github.com/nDmitry/grunt-postcss
		postcss: {
			options: {
				map: false,
				processors: [
					require('autoprefixer')({browsers: ['last 2 versions']}.postcss)
				]
			},

			development: {
				files: {
					'<%= config.cssDestFile %>':'<%= config.cssDestFile %>'
				}
			},

			distribution: {
				files: {
					'<%= config.cssDestFile %>':'<%= config.cssDestFile %>'
				}
			}
		},

		// Task for minifing the css file(s) 
		// ---------------------------------------------------------------------
		// Reference: https://github.com/gruntjs/grunt-contrib-cssmin
		cssmin: {
			options: {
				compatibility: 'ie9',
				keepSpecialComments: '*',
				sourceMap: false,
				advanced: false
			},

			distribution: {
				files: {
					'<%= config.cssDestFileMinified %>':'<%= config.cssDestFile %>'
				}
			}
		},

		// Task for concatenating the HTML partial files 
		// ---------------------------------------------------------------------
		// Reference: https://github.com/gruntjs/grunt-contrib-concat
		concat: {
			prototype: {
				files: {
					// Create Homepage
					'<%= config.htmlDestDir %>/index.html' :
					 [
						'<%= config.htmlSrcDir %>/_html-pre.html',
						'<%= config.htmlSrcDir %>/_html-head.html',
						'<%= config.htmlSrcDir %>/_region-header.html',
						'<%= config.htmlSrcDir %>/_region-navigation.html',
						'<%= config.htmlSrcDir %>/_region-main-sb-no.html',
						'<%= config.htmlSrcDir %>/_region-footer.html',
						'<%= config.htmlSrcDir %>/_html-post.html',
					],
					
					// Create Theme with sidebar on the left side
					'<%= config.htmlDestDir %>/sb-left.html' :
					 [
						'<%= config.htmlSrcDir %>/_html-pre.html',
						'<%= config.htmlSrcDir %>/_html-head.html',
						'<%= config.htmlSrcDir %>/_region-header.html',
						'<%= config.htmlSrcDir %>/_region-navigation.html',
						'<%= config.htmlSrcDir %>/_region-main-sb-left.html',
						'<%= config.htmlSrcDir %>/_region-footer.html',
						'<%= config.htmlSrcDir %>/_html-post.html',
					],

					// Create Theme with sidebar on the right side
					'<%= config.htmlDestDir %>/sb-right.html' :
					 [
						'<%= config.htmlSrcDir %>/_html-pre.html',
						'<%= config.htmlSrcDir %>/_html-head.html',
						'<%= config.htmlSrcDir %>/_region-header.html',
						'<%= config.htmlSrcDir %>/_region-navigation.html',
						'<%= config.htmlSrcDir %>/_region-main-sb-right.html',
						'<%= config.htmlSrcDir %>/_region-footer.html',
						'<%= config.htmlSrcDir %>/_html-post.html',
					],
				}
			}
		},

		// Task to replace some markup for the generated files from above
		// ---------------------------------------------------------------------
		// Reference: https://github.com/bomsy/grunt-"regex-replace"
		"regex-replace": {
			index: {
				src: '<%= config.htmlDestDir %>/index.html',
				actions: [
					{
						name: 'navActive',
						search: '<a class="nav-link nav-link_f4" href="index.html">',
						replace: '<a class="nav-link nav-link_f4 active" href="index.html">',
						flags: ''
					}
				]
			},

			sidebarLeft: {
				src: '<%= config.htmlDestDir %>/sb-left.html',
				actions: [
					{
						name: 'navActive',
						search: '<a class="nav-link nav-link_f4" href="sb-left.html">',
						replace: '<a class="nav-link nav-link_f4 active" href="sb-left.html">',
						flags: ''
					}
				]
			},

			sidebarRight: {
				src: '<%= config.htmlDestDir %>/sb-right.html',
				actions: [
					{
						name: 'navActive',
						search: '<a class="nav-link nav-link_f4" href="sb-right.html">',
						replace: '<a class="nav-link nav-link_f4 active" href="sb-right.html">',
						flags: ''
					}
				],
			},

			navDropdown: {
				src: '<%= config.htmlDestDir %>/typography.html',
				actions: [
					{
						name: 'navActive',
						search: 'nav-link nav-link_f4 dropdown-toggle',
						replace: 'nav-link nav-link_f4 dropdown-toggle active',
						flags: ''
					},

					{
						name: 'subNavActive',
						search: '<a class="dropdown-item dropdown-item_f4" href="typography.html">',
						replace: '<a class="dropdown-item dropdown-item_f4 active" href="typography.html">',
						flags: ''
					}
				]
			}
		},

		// Task for automation 
		// ---------------------------------------------------------------------
		watch: {
			development: {
				// '*' is used to include subdirectories
				// '**' Would be the regular wildcard to include subdirectories
				files: '<%= config.scssSrcDir %>/**/*.scss',
				
				// Reference to the defined tasks
				// -> Compile SCSS and process Post CSS
				tasks: [
					'sass:development',
					'postcss:development'
				]
			},

			prototype: {
				// '*' is used to include subdirectories
				// '**' Would be the regular wildcard to include subdirectories
				files: '<%= config.htmlSrcDir %>/**/*.html',
				
				// Reference to the defined tasks
				// -> Build the HTML Prototype
				tasks: [
					'concat:prototype',
					'regex-replace:index',
					'regex-replace:sidebarLeft',
					'regex-replace:sidebarRight',
					'regex-replace:navDropdown'
				]
			}
		}
	});

	// Create the tasks
	// -------------------------------------------------------------------------
	// Reference: https://github.com/sindresorhus/load-grunt-tasks

	// Load the dependencies first
	require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

	// Task/Job for Development
	grunt.registerTask('development', [
		'sass:development',
		'postcss:development'
	]);

	// Task/Job for Distribution/Production
	grunt.registerTask('distribution', [
		'sass:distribution',
		'postcss:distribution',
		'cssmin:distribution'
	]);

	// Build the HTML Prototype by concating the html files
	grunt.registerTask('prototype', [
		'concat:prototype',
		'regex-replace:index',
		'regex-replace:sidebarLeft',
		'regex-replace:sidebarRight',
		'regex-replace:navDropdown'
	])
};
