# Folder vendors

All projects will have a folder `vendors` containing the files from external libraries and frameworks:

* Bootstrap, version 4 alpha 2
* Font Awesome, version 4.5.0

:point_right: If you have to override settings, don't do it here!

# Customizing Bootstrap
Imports: `/scss/settings/_settings.bootstrap.imports.scss`
Variables: `/scss/settings/_settings.bootstrap.custom.scss`

# Customizing Font Awesome
Imports: `/scss/settings/_settings.font-awesome.imports.scss`
